# **PLOTJS**

This is a little library for drawing a line plot

### **Plot(can,points, name)**

Function for drawing a line plot

Input arguments:
- can -  DOMElement of canvas tag
- points - object with constructor Coordinates, contains two-dimensional
 Array, that contains coordinates of points [x,y]
- name - string, name of plot

examples of usage:

`var cord = new Coordinates(x,y);`

`var N = new Plot(canv,cord,'plot');`

There is 4 examples in index.html with different datasets
    
