function Coordinates(x, y) {
    this.length = 0;
    this.points = [];
    this.Xneg = 0;
    this.Xpos = 0;
    this.Yneg = 0;
    this.Ypos = 0;
    if (x.length === y.length) {
        for (let i of y) {
            this.points.push([x[this.length], y[this.length]]);
            x[this.length] < 0 ? this.Xneg++ : this.Xpos++;
            y[this.length] < 0 ? this.Yneg++ : this.Ypos++;
            this.length++;
        }
    }
    else if (!x && y){
        for (let i of y) {
            this.points.push([this.length, y[this.length]]);
            x[this.length] < 0 ? this.Xneg++ : this.Xpos++;
            y[this.length] < 0 ? this.Yneg++ : this.Ypos++;
            this.length++;
        }
    }
    else if (x && !y){
        for (let i of x) {
            this.points.push([x[this.length], this.length]);
            x[this.length] < 0 ? this.Xneg++ : this.Xpos++;
            y[this.length] < 0 ? this.Yneg++ : this.Ypos++;
            this.length++;
        }
    }

    this.points.sort(function (a, b) {
        return a[0] - b[0];
    });

}

var g = new function Graph(){
    var storage = [];
    this.storageGraph = function (a){
        return storage.push(a);
    };
    this.getGraph = function (i){
        return storage[i];
    };
    this.getLength = function () {
        return storage.length;
    }
};

function Plot(can,points, name) {
    var con = can.getContext('2d');
    var data_points = [];
    if (function (obj = points) {return obj.constructor === Coordinates;}){
        data_points = points.points;
    }
    g.storageGraph(this);

    //Styles
    this.axisColor = 'white';
    this.font = "12pt Helvetica";
    con.lineWidth = 2;
    con.strokeStyle = 'orange';
    con.fillStyle = 'black';
    con.textBaseline = 'top';

    let find_min_max_y = function (p){
        let max = p[0][1];
        let min = p[0][1];
        let n = 0;
        for (let i of p){
            max = p[n][1] > max ? p[n][1] : max;
            min = p[n][1] < min ? p[n][1] : min;
            n++;
        }
        return [min,max];
    };
    let round10 = function(m){
        if (m==0) return m;
        let n = 0;
        let f = true;
        let g = true;
        if (m<0){m = Math.abs(m); g = false;}
        if (m<10) f=false;
        while (m<10){
            m*=10;
            n++;
        }
        while (m>=100){
            m/=10;
            n++;
        }
        m = Math.ceil(m);
        m = f ? m * Math.pow(10,n) :  m / Math.pow(10,n);
        return !g ? -m : m;
    };

    //Constants
    this.name = name;
    let data_pixels = [];
    let numOfTicks = 5;
    const padding = 20;
    const minX = data_points[0][0];
    const maxX = data_points[data_points.length-1][0];
    const minmax = find_min_max_y(data_points);
    const minY = minmax[0];
    const maxY = minmax[1];
    let rangeX = round10(maxX) - round10(minX);
    let rangeY = round10(maxY) - round10(minY);
    let width = can.width - padding*2;
    let height = can.height - padding*2;
    let scaleX = (width-padding*2) / rangeX;
    let scaleY = (height-padding*2) / rangeY;
    let negwidth =  Math.abs(minX)/rangeX * (width-padding*2);
    let negheight = Math.abs(minY)/rangeY * (height-padding*2);
    let poswidth = width - padding*2 - negwidth;
    let posheight = height - padding*2 - negheight;



    Plot.prototype.drawBackground = function() {
        con.fillRect(0,0,can.width,can.height);
    };

    Plot.prototype.drawYaxis = function() {
        con.save();
        con.beginPath();
        con.strokeStyle = this.axisColor;
        con.fillStyle = this.axisColor;
        con.moveTo(negwidth+padding, 0);
        con.lineTo(negwidth+padding, height);
        //arrow
        con.moveTo(negwidth+padding,height);
        con.lineTo(negwidth +padding - 15,height - 15);
        con.moveTo(negwidth+padding,height);
        con.lineTo(negwidth +padding + 15,height - 15);
        //ticks
        let i = 0;
        let oneh;
        oneh = negheight != 0 ? negheight/numOfTicks : posheight/numOfTicks;
        let ith = oneh*i;
        while (ith < height-padding-10 && i < 1000){
            con.moveTo(negwidth+padding-10, padding + ith);
            con.lineTo(negwidth+padding+10, padding + ith);
            //ticks-labels
            con.scale(1,-1);
            (ith == negheight) ? con.fillText('', negwidth+padding+10, -negheight) :
                con.fillText(((ith - negheight)/scaleY).toFixed(2), negwidth+padding+10,-(ith+20));
            con.scale(1,-1);
            ith = oneh*(++i);
        }
        con.stroke();
        con.restore();
    };
    Plot.prototype.drawXaxis = function() {
        con.save();
        con.beginPath();
        con.strokeStyle = this.axisColor;
        con.fillStyle = this.axisColor;
        con.moveTo(0,negheight +padding);
        con.lineTo(width, negheight +padding);
        //arrow
        con.moveTo(width,negheight + padding);
        con.lineTo(width - 15,negheight +padding - 15);
        con.moveTo(width ,negheight +padding);
        con.lineTo(width - 15,negheight +padding + 15);
        //ticks
        let i = 0;
        let onew = negwidth != 0 ? negwidth/numOfTicks : poswidth/numOfTicks;
        let itw = onew*i;
        while (itw < width-padding-10 && i < 1000){
            con.moveTo(padding + itw, negheight + padding - 10);
            con.lineTo(padding + itw, negheight + padding + 10);
            //ticks-labels
            con.scale(1,-1);
            (itw == negwidth) ? con.fillText(0, itw+30,-(negheight + padding - 10)) :
                con.fillText(((itw - negwidth)/scaleX).toFixed(2), itw+10,-(negheight + padding - 10));
            con.scale(1,-1);
            itw = onew*(++i);
        }
        con.stroke();
        con.restore();
    };

    Plot.prototype.convertToPixels = function () {
        for (let i of data_points){
            data_pixels.push([Math.floor(i[0]*scaleX),Math.floor(i[1]*scaleY)]);
        }
    };

    Plot.prototype.drawLine = function () {
        con.save();
        con.beginPath();
        let j = 0;
        con.moveTo(data_pixels[j][0],data_pixels[j][1]);
        j++;
        while (data_pixels[j] !== undefined){
            con.lineTo(data_pixels[j][0],data_pixels[j][1]);
            j++;
        }
        con.stroke();
        con.closePath();
        con.restore();
    };

    Plot.prototype.drawPoints = function () {
        con.save();
        con.fillStyle = con.strokeStyle;
        let j = 0;
        while (data_pixels[j] !== undefined){
            con.beginPath();
            con.arc(data_pixels[j][0],data_pixels[j][1],5,0,Math.PI*2);
            con.fill();
            j++;
        }
        con.restore();
    };

    this.drawBackground();
    con.translate(padding,height+padding);
    con.scale(1,-1);
    this.drawXaxis();
    this.drawYaxis();
    con.translate(negwidth+padding,negheight+padding);
    this.convertToPixels();
    this.drawLine();
    if (data_points.length < 1000) this.drawPoints();
}
// var P = new Plot(canvas,cord,"test");
